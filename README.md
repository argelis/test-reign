<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

# hacker API

1.- Clonar proyecto

2.- ```npm install```

3.- Clonar el archivo .env.template y renombrarlo a .env

4.- Cambiar las variables de entorno

5.- Levantar la BD
```
docker-compose up -d
```

6.- levantar el modo desarrollo ```npm run start:dev```