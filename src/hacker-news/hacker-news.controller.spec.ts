import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { HackerNew } from './entities/hacker-new.entity';
import { HackerNewsController } from './hacker-news.controller';
import { HackerNewsService } from './hacker-news.service';

describe('HackerNewsController', () => {
  let controller: HackerNewsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HackerNewsController],
      providers: [HackerNewsService,
        {
          provide: getRepositoryToken(HackerNew),
          useValue: {
            save: jest.fn().mockResolvedValue({}),
            find: jest.fn().mockResolvedValue([{}]),
          },
        },],
    }).compile();

    controller = module.get<HackerNewsController>(HackerNewsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
