import { Injectable, InternalServerErrorException, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cron } from '@nestjs/schedule';
import { ILike, ArrayContains } from "typeorm";

import axios, { AxiosInstance } from 'axios';
import { HackerNewsResponse } from './interface/hacker-news.interface';
import { HackerNew } from './entities/hacker-new.entity';
import { PaginationDto } from 'src/common/dtos/pagination.dto';
import { ParamsDto } from 'src/common/dtos/params.dto';

@Injectable()
export class HackerNewsService {


  constructor(
    @InjectRepository(HackerNew)
    private readonly hackerNewsRepository: Repository<HackerNew>
  ) { }

  private readonly axios: AxiosInstance = axios;
  private readonly logger = new Logger(`create-${Date()}`);

  @Cron('* * 1 * * *')
  async createHackerNewCron() {
    try {
      const { data } = await this.axios.get<HackerNewsResponse>('https://hn.algolia.com/api/v1/search_by_date?query=nodejs');

      data.hits.forEach(async news => {
        const hackerNewCount = await this.hackerNewsRepository.count({
          where: { created_at_i: news.created_at_i },
          withDeleted: true
        })

        if (hackerNewCount == 0)
          await this.hackerNewsRepository.save({
            created_at_i: news.created_at_i,
            tags: news._tags,
            author: news.author,
            title: news.title,
          })

      });

      this.logger.debug('This action adds a new hackerNew');

    } catch (error) {

      this.logger.debug('Error create hackerNewsCron' + error);

    }
  }

  async findAll(paginationDto: PaginationDto, filterDto: ParamsDto) {

    const { limit , offset } = paginationDto
    const { title, author, tags } = filterDto
    const query = this.hackerNewsRepository

    let search = {};

    if (title)
      search = { title: ILike(`%${title}%`) }

    if (author)
      search = { ...search, author: ILike(`%${author}%`) }

    if (tags)
      search = { ...search, tags: ArrayContains([tags]) }

    const hackerNewsData = await query.find({
      take: limit,
      skip: offset,

      where: {
        ...search
      }

    })

    return hackerNewsData;
  }

  async findOne(id: string) {
    const hackerNew = await this.hackerNewsRepository.findOneBy({ id });

    if (!hackerNew)
      throw new NotFoundException(`Hacker News with id ${id} not found`)

    return hackerNew
  }

  async remove(id: string) {
    const hackerNews = await this.findOne(id)
    await this.hackerNewsRepository.softRemove(hackerNews)
  }
}
