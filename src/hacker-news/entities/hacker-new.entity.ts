import { Entity, PrimaryGeneratedColumn, Column, DeleteDateColumn } from 'typeorm';

@Entity()
export class HackerNew {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('varchar', {
        nullable: true
    })
    title: string;

    @Column('varchar', {
        nullable: true
    })
    author: string;

    @Column('text', {
        array: true,
        default: []
    })
    tags!: string[];

    @Column('integer', {
        unique: true
    })
    created_at_i: number;

    @DeleteDateColumn()
    deletedAt?: Date;
}
