import { Module } from '@nestjs/common';
import { HackerNewsService } from './hacker-news.service';
import { HackerNewsController } from './hacker-news.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HackerNew } from './entities/hacker-new.entity';

@Module({
  controllers: [HackerNewsController],
  providers: [HackerNewsService],
  imports: [
    TypeOrmModule.forFeature([HackerNew])
  ]
})
export class HackerNewsModule { }
