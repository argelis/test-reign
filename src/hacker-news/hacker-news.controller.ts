import { Controller, Get, Param, Delete, ParseUUIDPipe, Query } from '@nestjs/common';
import { PaginationDto } from './../common/dtos/pagination.dto';
import { ParamsDto } from './../common/dtos/params.dto';
import { HackerNewsService } from './hacker-news.service';

@Controller('hacker-news')
export class HackerNewsController {
  constructor(private readonly hackerNewsService: HackerNewsService) { }

  @Get()
  findAll(@Query() paginationDto: PaginationDto, @Query() filterDto: ParamsDto) {
    return this.hackerNewsService.findAll(paginationDto, filterDto);
  }

  @Delete(':id')
  remove(@Param('id', ParseUUIDPipe) id: string) {
    return this.hackerNewsService.remove(id);
  }
}
