import { Type } from 'class-transformer';
import { IsOptional, IsPositive, Max, Min } from 'class-validator';

export class PaginationDto {
    
    @IsPositive()
    @Max(5)
    @Type( () => Number ) // enableImplicitConversions: true
    limit: number;
    
    @Min(0)
    @Type( () => Number ) // enableImplicitConversions: true
    offset?: number;
}