import { IsOptional, IsPositive, Min } from 'class-validator';

export class ParamsDto {
    @IsOptional()
    title?: string;

    @IsOptional()
    author?: string;

    @IsOptional()
    tags?: string;

}